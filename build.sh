#!/bin/bash
cd build

sh updateDepencies.sh
sh buildDepencies.sh
sh buildCore.sh
sh buildServer.sh
sh buildEngine.sh
sh buildGame.sh

