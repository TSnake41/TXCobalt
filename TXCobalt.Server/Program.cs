﻿/*
 *  This file is a part of TXCobalt server.
 *  Copyright (C) 2015 Astie Teddy
 *  This file is under the MIT licence, you should have recieved a copy of the licence,
 *  if you do not have recieved an copy of the licence follow this link :
 *  https://opensource.org/licenses/MIT
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using TXCobalt.Core;
using TXCobalt.Core.NMS;

namespace TXCobalt.Server
{
    class Program
    {
        public static IPAddress ListeningIP
        {
            get
            {
                return IPAddress.Parse(Settings["IP"]);
            }
        }

        public static Socket Listner;
        public static GameRules rules;
        public static GameInstanceManager manager;
        public static Dictionary<string, string> Settings;

        public static bool UsePassword
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Settings["Password"]);
            }
        }

        public static string MapName;

        static void Main()
        {
            /*
                InstanceSettings settings = new InstanceSettings("rules.json", "map.txmap", new NmsSettings(),
                    new Dictionary<string, object> { { "DedicatedHost", "host" }, { "DedicatedPort", 5050 } }, "Guid", "Manager");

                var writer = new LitJson.JsonWriter(new StreamWriter("multiInstance.json"));
                writer.PrettyPrint = true;
                LitJson.JsonMapper.ToJson(settings, writer);
                writer.TextWriter.Dispose();
                return;
            */

            // Use executable dir (FIX for Linux)
            Environment.CurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            Settings = ServerSettings.ReadSettings("settings.properties");
            Log.ShowDebug = bool.Parse(Settings["ShowDebug"]);
            Log.UseLogFile = bool.Parse(Settings["UseLog"]);

            Listner = new Socket(IPAddress.Parse(Settings["IP"]).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            MapName = Path.GetFileNameWithoutExtension(Settings["MapPath"]);

            Console.Title = "TXCobalt dedicated server: " + ListeningIP + ':' + Settings["Port"];
            Log.Write("TXCobalt dedicated server 0.2.");
            Log.Write(TXCobalt.Core.Locale.Current["server-rules-import"]);

            #region Rules
            if (File.Exists(Settings["RulesPath"]))
                rules = GameRules.ImportGameRules(Settings["RulesPath"]);
            else
            {
                GameRules.ExportGameRules("default_rules.json", GameRules.Default);
                Log.Warning(TXCobalt.Core.Locale.Current["server-rules-notfound"]);
                rules = GameRules.Default;
            }
            #endregion
            Log.Write(TXCobalt.Core.Locale.Current["server-game-init"]);
            Log.Write("MaxPlayer={0} MaxGameObject={1}", rules.MaxPlayer, rules.MaxGameObject);

            manager = new GameInstanceManager();
            manager.AddServer(new GameInstance(Map.ImportMap(Settings["MapPath"]),
                GameRules.ImportGameRules(Settings["RulesPath"]),
                new NmsSettings(int.Parse(Settings["NmsUpdateTime"]), int.Parse(Settings["NmsMaxMessages"]))));

            try
            {
                Listner.Bind(new IPEndPoint(IPAddress.Parse(Settings["IP"]), int.Parse(Settings["Port"])));
            }
            catch
            {
                Log.Error(TXCobalt.Core.Locale.Current["server-portbind-failed"]);
                Thread.CurrentThread.Join();
            }

            Listner.Listen(20);
            Log.Write(TXCobalt.Core.Locale.Current["server-connection-wait"]);
            while (true)
            {
                Socket client = Listner.Accept();
                client.ReceiveTimeout = 3000;
                client.SendTimeout = 1250;
                try
                {
                    IPAddress address = ((IPEndPoint)client.RemoteEndPoint).Address;

                    Log.Write(string.Format(TXCobalt.Core.Locale.Current["server-socket-accept"], address));
                    Log.Write(TXCobalt.Core.Locale.Current["server-handshake-send"]);
                    Log.Write("Server map: " + MapName);
                    client.Send(new ServerResponse
                    {
                        PlayerCount = manager.Players.Count,
                        IsAvailable = true,
                        Map = Settings["MapPath"],
                        MaxPlayer = manager[0].Rules.MaxPlayer,
                        Moded = false,
                        MOTD = Settings["MOTD"],
                        PasswordProtected = !string.IsNullOrWhiteSpace(Settings["Password"]),
                        ProtocolVersion = "dev-0.2.1",
                        UseAlternateSerialization = false
                    }.Serialize());

                    Thread.Sleep(1250);
                    Log.Write(TXCobalt.Core.Locale.Current["server-handshake-wait"]);
                    byte[] datas = new byte[2048];
                    int Length = client.Receive(datas);
                    Array.Resize(ref datas, Length);
                    ConnectionRequest request = Serializer.Deserialize<ConnectionRequest>(datas);
                    if (!string.IsNullOrEmpty(request.Username) && !manager.IsPresent(request.Username) && (!UsePassword || request.Password == Settings["Password"]))
                        manager[0].SendRequest(client, request);
                    else
                    {
                        Log.Write(TXCobalt.Core.Locale.Current["server-handshake-deny"], address);
                        client.Close(500);
                    }
                }
                catch (Exception e)
                {
                    client.Close();
                    Log.Error(TXCobalt.Core.Locale.Current["server-error"], e.ToString());
                }
            }
        }
    }
}
