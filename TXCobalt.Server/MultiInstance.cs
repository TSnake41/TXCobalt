﻿/*
 *  This file is a part of TXCobalt server.
 *  Copyright (C) 2015 Astie Teddy
 *  This file is under the MIT licence, you should have recieved a copy of the licence, 
 *  if you do not have recieved an copy of the licence follow this link : 
 *  https://opensource.org/licenses/MIT
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using TXCobalt.Core;
using TXCobalt.Core.NMS;
using System.Net;
using System.Net.Sockets;

namespace TXCobalt.Server
{
    public class MultiInstance
    {
        public Dictionary<Guid, ServerEntry> Entries;
        public ServerInstance[] Instances;

        public static MultiInstance CreateFromFile(string file)
        {
            if(!File.Exists(file))
                throw new FileNotFoundException("The properties file was not found.", file);

            var mi = new MultiInstance();
            return mi;
        }

    }

    public struct EntrySettings
    {
        public EntrySettings(string motd, string password, Guid guid)
        {
            MOTD = motd;
            Password = password;
            GUID = guid;
        }

        public string MOTD, Password;
        public Guid GUID;

        public bool UsePassword
        { 
            get { return !String.IsNullOrWhiteSpace(Password); }
        }
    }

    public class ServerInstance
    {
        public ServerInstance(string mapfile, string rulefile, NmsSettings nmsSetting)
        {
            Instance = new GameInstance(Map.ImportMap(mapfile), GameRules.ImportGameRules(rulefile), nmsSetting);
            InstanceThread = new Thread(Instance.Loop);

            InstanceThread.Start();
        }

        public bool Frozen { get { return Instance.Frozen; } }
        public bool Active { get { return Instance.Active; } }

        public GameInstance Instance;
        public Thread InstanceThread;
    
        public void Start()
        {
            Instance.Frozen = false;

            if (Instance.Active)
                Log.Warning("Could not start an already active instance.");
            else
            {
                Instance.Active = true;
                if (InstanceThread != null)
                    InstanceThread.Start();
            }
        }

        public void Stop()
        {
            Instance.Active = false;
        }

        public void ToggleFreeze()
        {
            Instance.Frozen = !Instance.Frozen;
        }
    }

    public class ServerEntry
    {
        public ServerEntry(GameInstanceManager manager, EntrySettings settings, EndPoint endpoint)
        {
            Listner = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            Listner.Bind(endpoint);
            Listner.Listen(50);

            Settings = settings;

            ListeningThread = new Thread(() =>
            {
                while (true)
                {
                    Socket client = Listner.Accept();
                    client.ReceiveTimeout = 3000;
                    client.SendTimeout = 1250;
                    try
                    {
                        IPAddress address = ((IPEndPoint)client.RemoteEndPoint).Address;

                        Log.Write(string.Format("Socket connection accepted for: {0} on instance {1} ({2})", address, settings.GUID, settings.MOTD));
                        Log.Write("Send to client : server informations.");
                        Log.Write("Server map: " + manager[0].map);
                        client.Send(MakeResponse().Serialize());

                        Thread.Sleep(1250);
                        Log.Write("Wait for UserData ...");
                        var datas = new byte[2048];
                        int Length = client.Receive(datas);
                        Array.Resize(ref datas, Length);
                        ConnectionRequest request = Serializer.Deserialize<ConnectionRequest>(datas);
                        if (!string.IsNullOrEmpty(request.Username) && !manager.IsPresent(request.Username) && (!Settings.UsePassword || request.Password == Settings.Password))
                            manager[0].SendRequest(client, request);
                        else
                        {
                            Log.Write("Connection refusée pour {0}", address);
                            client.Close(500);
                        }
                    }
                    catch (Exception e)
                    {
                        client.Close();
                        Log.Error("Erreur: {0}", e.ToString());
                    }
                }
            });
            Console.WriteLine("Instance {0} started.", Settings.GUID);
        }

        public GameInstanceManager Manager;
        public Socket Listner;
        public Thread ListeningThread;
        public EntrySettings Settings;

        public string MapName;

        public void AddInstance(ServerInstance instance)
        {
            Manager.AddServer(instance.Instance);
        }

        ServerResponse MakeResponse()
        {
            return new ServerResponse 
            {
                PlayerCount = Manager.Players.Count, 
                IsAvailable = true,
                Map = MapName,
                MaxPlayer = Manager[0].Rules.MaxPlayer,
                Moded = false,
                MOTD = Settings.MOTD, 
                PasswordProtected = Settings.UsePassword,
                ProtocolVersion = "dev-0.2.1",
                UseAlternateSerialization = false 
            };
        }
    }
}