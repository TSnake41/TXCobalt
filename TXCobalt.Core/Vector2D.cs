﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Core
{
    [Serializable]
    public struct Vector2D
    {
        public int x, y;

        public Vector2D(int x, int y)
        {
            this = new Vector2D { x = x, y = y };
        }

        public override string ToString()
        {
            return "(" + x + ";" + y + ")";
        }

        #region Operators

        public static Vector2D operator +(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.x + v2.x, v1.y + v2.y);
        }

        public static Vector2D operator -(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.x - v2.x, v1.y - v2.y);
        }

        public static Vector2D operator *(Vector2D v1, int m)
        {
            return new Vector2D(v1.x * m, v1.y * m);
        }

        public static Vector2D operator *(Vector2D v1, float m)
        {
            return new Vector2D((int)(v1.x * m), (int)(v1.y * m));
        }

        public static float operator *(Vector2D v1, Vector2D v2)
        {
            return v1.x * v2.x + v1.y * v2.y;
        }

        public static Vector2D operator /(Vector2D v1, int m)
        {
            return new Vector2D(v1.x / m, v1.y / m);
        }

        public static Vector2D operator /(Vector2D v1, float m)
        {
            return new Vector2D((int)(v1.x / m), (int)(v1.y / m));
        }

        public static bool operator !=(Vector2D v1, Vector2D v2)
        {
            return v1.x != v2.x || v1.y != v2.y;
        }

        public static bool operator ==(Vector2D v1, Vector2D v2)
        {
            return v1.x == v2.x && v1.y == v2.y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2D))
                return false;
            Vector2D Obj = (Vector2D)obj;
            return x == Obj.x && y == Obj.y;
        }
        public override int GetHashCode()
        {
            return (int)(-2147483648L + x << 16 + y);
        }
        #endregion
    }
}