﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Core
{
    /// <summary>
    /// Contient toutes les données requises pour la connection d'un client.
    /// </summary>
    [Serializable]
    public struct ConnectionRequest
    {
        public ConnectionRequest(string username, string password = "", string version = "Unspecified", Color? color = null)
        {
            Username = username;
            Password = password;
            argbcolor = color.HasValue ? color.Value.ToArgb() : new Color(200, 200, 200).ToArgb();
            Version = version;
        }

        public string Username;
        /// Used only if server need this.
        public string Password;
        public int argbcolor;
        public Color color
        {
            get { return Color.FromArgb(argbcolor); }
            set { argbcolor = value.ToArgb(); }
        }

        public string Version;
    }
}