﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using LitJson;

namespace TXCobalt.Core
{
    /// <summary>
    /// La réponse du server, il est recommandé qu'il soit sérialisé en JSON.
    /// </summary>
    [Serializable]
    public struct ServerResponse
    {
        public string MOTD;
        public string Map;
        public bool Moded;

        public bool PasswordProtected;

        public bool IsAvailable;
        public int PlayerCount;
        public int MaxPlayer;

        public string ProtocolVersion;
        public bool UseAlternateSerialization;

        public byte[] Serialize()
        {
            return (Encoding.UTF8.GetBytes(JsonMapper.ToJson(this)));
        }

        public static ServerResponse Deserialize(byte[] datas)
        {
            Console.WriteLine(Encoding.UTF8.GetString(datas));
            return JsonMapper.ToObject<ServerResponse>(Encoding.UTF8.GetString(datas));
        }
    }
}
