/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

namespace TXCobalt.Core.NMS
{
	public struct NmsSettings
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="TXCobalt.Core.NMS.NmsSettings"/> struct.
        /// </summary>
        /// <param name="CheckingDelay">The sleeping time between each checking. (in ms)</param>
        /// <param name="MaxProcessedMessages">The maximum of processed message per client on checking.</param>
        public NmsSettings(int checkingDelay = 50, int maxProcessedMessages = 8)
        {
            CheckingDelay = checkingDelay;
            MaxProcessedMessages = maxProcessedMessages;
        }

        /// <summary>
        /// The sleeping time between each checking. (in ms)
        /// </summary>
        /// <remarks>
        /// Set to 0, there was no delay.
        /// 
        /// </remarks>
        public int CheckingDelay;

        /// <summary>
        /// The maximum of processed message per client on checking.
        /// </summary>
        public int MaxProcessedMessages;
	}
}