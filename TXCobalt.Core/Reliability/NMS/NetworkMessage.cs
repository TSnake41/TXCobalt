﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// The NSM (network message system) is an fast and TCP-optimised packet transport, he can send more informations (like chat or rcon) using less packets.
using System;
using System.Collections.Generic;

namespace TXCobalt.Core.NMS
{
    [Serializable]
    public struct NetworkMessage
    {
        public NetworkMessage(MessageFlag flag, object data)
        {
            Flag = flag;
            Data = data;
        }

        /// <summary>
        /// Datas type used for each MessageFlag.
        /// </summary>
        public static Dictionary<MessageFlag, Type> DataTypes = new Dictionary<MessageFlag, Type>()
        {
            { MessageFlag.ERROR, typeof(ErrorMessage) },
            { MessageFlag.Update, typeof(GameUpdate) },
            { MessageFlag.InputData, typeof(InputData) },
            { MessageFlag.ChatIn, typeof(string) },
            { MessageFlag.ChatOut, typeof(string[]) },
            { MessageFlag.RCONIn, typeof(string) },
            { MessageFlag.RCONOut, typeof(string[]) },
            { MessageFlag.DISCONNECT, typeof(byte) },
            { MessageFlag.KICK, typeof(KickMessage) }
        };

        /// <summary>
        /// Custom action for an Message flag id.
        /// </summary>
        public static Dictionary<int, Action<object>> CustomObjectActions = new Dictionary<int, Action<object>>();

        public MessageFlag Flag;
        public object Data;

        public static byte[] Serialize(NetworkMessage message)
        {
            /*
            if (!Serializer.UseAlternativeSerializer)
            {
                Type type = DataTypes[message.Flag];

                if(type.IsArray)
                    message.Data = ((IEnumerable)message.Data).Cast<object>().ToArray();
                else if (!type.IsPrimitive && type != typeof(string))
                    // Convert to dictionnary if needed.
                    message.Data = ReflectionUtilities.ToDictionnary(message.Data);
            }
            */
            byte[] datas = Serializer.Serialize(message);
            return datas;
        }

        public static NetworkMessage Deserialize(byte[] datas)
        {
            NetworkMessage message = Serializer.Deserialize<NetworkMessage>(datas);

            /*if (!Serializer.UseAlternativeSerializer)
            {
                if (DataTypes[message.Flag].IsArray)
                    message.Data = ReflectionUtilities.ToArray((object[])message.Data, DataTypes[message.Flag].GetElementType());
                else if (!(DataTypes[message.Flag].IsPrimitive || DataTypes[message.Flag] == typeof(string) || DataTypes[message.Flag].IsEnum))
                    message.Data = ReflectionUtilities.ToObject((Dictionary<string,object>)message.Data, DataTypes[message.Flag]);

                if (message.Flag == MessageFlag.Update)
                {
                    GameUpdate update = (GameUpdate)message.Data;
                    GameObjectContainer[] newcontainers = new GameObjectContainer[update.gameobjects.Length];
                    for (int i = 0; i < update.gameobjects.Length; i++)
                        newcontainers[i] = GameObjectContainer.Resolve(update.gameobjects[i]);
                    update.gameobjects = newcontainers;
                    message.Data = update;
                }
            }
            else
            {
                //TODO: Add Msgpack deserializer
            }
*           */
            return message;
        }

    }
}
