﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using NS = NetSerializer;
using TXCobalt.Core.NMS;

namespace TXCobalt.Core
{
    /// <summary>
    /// Fourni un serializer commun.
    /// </summary>
    public static class Serializer
    {
        /// <summary>
        /// Offset of all flags for NetSerializer serialization.
        /// </summary>
        public const uint FlagOffset = 32768;
        // 2 power 15

        static int nmsTypeCount;

        static Serializer()
        {
            InitNmsSerializer();
        }

        // public static bool UseAlternativeSerializer = false;
        public static bool KeepSerializers = true;

        /*public static SerializationContext MsgpackContext = new SerializationContext(PackerCompatibilityOptions.Classic)
        {
            EnumSerializationMethod = EnumSerializationMethod.ByUnderlyingValue,
            SerializationMethod = SerializationMethod.Map,
            GeneratorOption = SerializationMethodGeneratorOption.Fast
        };*/
        static NS.Serializer NmsSerializer;

        public static byte[] Serialize<T>(T Object, params Type[] additionnaltypes)
        {
            using (MemoryStream stream = new MemoryStream())
                {
                    if (Object is NetworkMessage)
                    {
                        if (nmsTypeCount != NetworkMessage.DataTypes.Count)
                            InitNmsSerializer();

                        NmsSerializer.Serialize(stream, Object);
                    }
                    else
                    {
                        List<Type> types = new List<Type> { typeof(T) };
                        types.AddRange(additionnaltypes);

                        var serializer = new NetSerializer.Serializer(types);

                        if (Object is GameObjectContainer)
                        {
                            GameObjectContainer obj = (GameObjectContainer)Convert.ChangeType(Object, typeof(GameObjectContainer));
                            stream.Position = 0;
                            obj = Abstract(obj);

                            serializer.Serialize(stream, obj);
                            return stream.ToArray();
                        }
                        else if (Object is GameUpdate)
                            {
                                GameUpdate update = (GameUpdate)Convert.ChangeType(Object, typeof(GameUpdate));
                                for (int i = 0; i < update.gameobjects.Length; i++)
                                    update.gameobjects[i] = Abstract(update.gameobjects[i]);
                                Object = (T)Convert.ChangeType(update, typeof(T));
                            }

                        stream.Position = 0;
                        serializer.Serialize(stream, Object);
                    }
                    return stream.ToArray();
                }
        }

        public static T Deserialize<T>(byte[] datas, params Type[] additionnaltypes)
        {
            using (MemoryStream stream = new MemoryStream(datas))
                /*if (UseAlternativeSerializer) // Use Msgpack serializer.
                {
                    SerializationContext context = new SerializationContext(PackerCompatibilityOptions.Classic);
                    return context.GetSerializer<T>().Unpack(stream);
                }
                else */// Use NetSerializer.
                {
                    if (typeof(T) == typeof(NetworkMessage))
                    {
                        if (nmsTypeCount != NetworkMessage.DataTypes.Count)
                            InitNmsSerializer();
                        
                        return (T)NmsSerializer.Deserialize(stream);
                    }
                    else
                    {
                        List<Type> types = new List<Type> { typeof(T) };
                        types.AddRange(additionnaltypes);

                        NS.Serializer serializer = new NetSerializer.Serializer(types);
                        return (T)serializer.Deserialize(stream);
                    }
                }
        }

        public static object Deserialize(byte[] datas, Type t)
        {
            using (MemoryStream stream = new MemoryStream(datas))
                /*if (UseAlternativeSerializer) // Use Msgpack serializer.
                    return MsgpackContext.GetSerializer(t).Unpack(stream);
                else // Use NetSerializer.*/
                {
                    if (t == typeof(NetworkMessage))
                    {
                        if (nmsTypeCount != NetworkMessage.DataTypes.Count)
                            InitNmsSerializer();
                        
                        return NmsSerializer.Deserialize(stream);
                    }
                    var serializer = new NS.Serializer(new [] { t, typeof(Dictionary<string, object>) });
                    return serializer.Deserialize(stream);
                }
        }

        public static GameObjectContainer Abstract(GameObjectContainer container)
        {
            GameObjectContainer new_container = container;
            object obj = container.ObjectData;

            new_container.ObjectData = ReflectionUtilities.ToDictionnary(obj);

            return new_container;
        }

        static void InitNmsSerializer()
        {
            nmsTypeCount = NetworkMessage.DataTypes.Count;

            var types = new List<Type> { typeof(NetworkMessage) };
            types.InsertRange(1, NetworkMessage.DataTypes.Values); 

            // Add GameObjects
            types.InsertRange(types.Count, GameObjectContainer.RegisteredValues.Values);

            NmsSerializer = new NS.Serializer(types.ToArray());
        }
    }
}