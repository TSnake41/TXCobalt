/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace TXCobalt.Core
{
    public struct Locale
    {
        public string this[string translation]
        {
            get
            {
                return Translations.ContainsKey(translation) ? Translations[translation] : EnglishLocale.Translations[translation];
            }
        }

        static Locale()
        {
            try
            {
                Current = new Locale(string.Format("locales{0}{1}.lang", Path.DirectorySeparatorChar, CultureInfo.CurrentCulture.Name));
            }
            catch
            {
                Log.Warning("Locale file {0} not found, use default locale (EN).", CultureInfo.CurrentCulture.Name);
                Current = EnglishLocale;
            }
        }

        public Locale(string localefile = "")
        {
            if (File.Exists(localefile))
            {
                var dictionary = new Dictionary<string, string>();
                string[] lines = File.ReadAllLines(localefile);
                foreach (string line in lines)
                {
                    if (line.StartsWith("#") || !line.Contains("="))
                        continue;

                    string[] keyvalue = line.Split(new []{ '=' }, 2);
                    dictionary.Add(keyvalue[0], keyvalue[1]);
                }
                this = new Locale(dictionary);
            }
            else
            {
                Log.Warning("Locale file {0} not found, use default locale (EN).", CultureInfo.CurrentCulture.Name);
                this = EnglishLocale;
            }
        }

        public Locale(Dictionary<string, string> translations)
        {
            Translations = translations;
        }

        public static Locale Current;
        Dictionary<string, string> Translations;

        public static Locale EnglishLocale = new Locale
        {
            Translations = new Dictionary<string, string>
            {
                // TXCobalt.Core

                ["log-file-unavailable" ] = "Log file unavaiable.",
                ["nms-client-msgrecieved" ] = "Message recieved {0} {1}.",
                ["nms-client-nodata" ] = "[NMS] Server has no datas !",
                ["nms-client-lostconnection" ] = "[NMS] Socket is not connected anymore.",

                ["gameinstance-player-notfound" ] = "The player is not found in objects.",
                ["gameinstance-playerobject-notfound" ] = "The player object is not found in objects.",
                ["gameinstance-whereis-nolocalisation" ] = "The Object or Player is located anywhere.",
                ["gameinstance-objectlimit-reached" ] = "Unable to instanciate object {0}, max object limit reached.",
                ["gameinstance-connection-toomanyplayers" ] = "Connection denied for {0}, there are too many already connected players.",
                ["gameinstance-connection-accepted" ] = "Connection accepted for {0} with {1} version (#{2})",
                ["gameinstance-update-called" ] = "Update called !",
                ["gameinstance-update-entityerror" ] = "Unable to update entities ? {0}",
                ["gameinstance-update-error" ] = "An error occured in Update()",
                ["gameinstance-playerkicked" ] = "The player {0} has been kicked for: {1}",

                ["gamerules-import-error" ] = "Failed to Import game rules, use default gamerules, {0}.",
                ["gamerules-export-error" ] = "Failed to Export game rules, {0}.",

                ["map-bounds-mismatch"] = "Map map Length or Height not corresponding to data array !",
                ["map-invalid"] = "The map {0} is unavailable or invalid, use an empty map.",
                ["map-export-error"] = "Can't export map at path {0}",

                ["mapio-invalid-header"] = "The map have an invalid signature/header, maybe is the map invalid or corrupted ?",
                ["mapio-import-notfound"] = "Map not found at path : {0}",

                // TXCobalt.Server
                ["server-rules-import"] = "Make or import rules: ",
                ["server-rules-notfound"] = "Could not find rules file, make rules",
                ["server-game-init"] = "Initialise Game Engine: ",
                ["server-portbind-failed"] = "Failed to bind to port !",
                ["server-connection-wait"] = "Waiting for connection ...",
                ["server-socket-accept"] = "Socket connection accepted for: {0}",
                ["server-handshake-send"] = "Send to client server informations.",
                ["server-handshake-wait"] = "Wait for UserData ...",
                ["server-handshake-deny"] = "Connection denied for {0}",
                ["server-error"] = "Error: {0}"
            }
        };
    }
}
