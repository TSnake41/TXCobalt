﻿/*
 *  Local player used for local or internal server.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
using System;

namespace TXCobalt.Core
{
    public class LocalPlayer : Player
    {
        public LocalPlayer(ConnectionRequest credidential)
        {
            GUID = Guid.NewGuid();
            Username = credidential.Username;
            color = credidential.color;
            Input = new InputData();
            Update = new GameUpdate { CameraPostion = new Vector2D(0, 0), gameobjects = new GameObjectContainer[] { }, Players = new PlayerData[] { } };
        }

        public InputData? Input;
        public GameUpdate Update;

        public override void Finish()
        {
            
        }
        public override InputData? GetInput()
        {
            return Input;
        }
        public override void SendUpdate(GameUpdate update)
        {
            Update = update;
        }
    }
}
