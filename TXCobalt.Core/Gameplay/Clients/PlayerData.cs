﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace TXCobalt.Core
{
    [Serializable]
    public struct PlayerData
    {
        public PlayerData(string username, int color, Guid guid)
        {
            Username = username;
            argbcolor = color;
            this.guid = guid.ToString();
        }
        public PlayerData(Player pl)
        {
            Username = pl.Username;
            argbcolor = pl.argbcolor;
            guid = pl.GUID.ToString();
        }

        public static PlayerData[] Pack(IEnumerable<Player> Players)
        {
            if (Players == null)
                throw new NullReferenceException("Players");

            List<PlayerData> Pls = new List<PlayerData>();

            foreach (Player pl in Players)
                Pls.Add(new PlayerData(pl));

            return Pls.ToArray();
        }

        public string Username;
        public int argbcolor;
        public string guid;
        public Guid GUID
        {
            get { return new Guid(guid); }
            set { guid = value.ToString(); }
        }
    }
}
