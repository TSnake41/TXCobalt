﻿/*
 *  Bullet object.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Core
{
    public class BulletObject : GameObject
    {
        public override object Data
        {
            get { return bulletdata; }
            set { bulletdata = (BulletObjectData)value; }
        }

        public BulletObjectData bulletdata;
        public override bool IsCollider
        {
            get
            {
                return false;
            }
        }
        public override string TypeId
        {
            get
            {
                return "Bullet";
            }
        }

        public override void Update(GameInstance instance)
        {
            for (int i = 0; i < instance.Rules.BulletSpeed; i++)
            {
                bool IsCollide;

                // Check if an collision.
                IsCollide = instance.TestCollision(transform.Pos.x, transform.Pos.y);

                // Apply damage to any present entities and destroy the bullet object if any.
                if (IsCollide)
                {
                    foreach (GameObject obj in instance.GetObjects(transform.Pos.x, transform.Pos.y))
                        if (obj is ITargetable && obj.GUID != bulletdata.SenderGuid)
                            ((ITargetable)obj).OnDamage(CalculateDamage((float)instance.Rules.BulletDamage));

                    // Destroy this.
                    instance.Destroy(this);
                    break;
                }

                Vector2D localmovepos = new Vector2D();
                Vector2D globalmovepos;

                switch (transform.tilt)
                {
                    case Tilt.Up:
                        localmovepos = new Vector2D(0, -1);
                        break;
                    case Tilt.Down:
                        localmovepos = new Vector2D(0, 1);
                        break;
                    case Tilt.Left:
                        localmovepos = new Vector2D(-1, 0);
                        break;
                    case Tilt.Right:
                        localmovepos = new Vector2D(1, 0);
                        break;
                }

                globalmovepos = localmovepos + transform.Pos;

                transform.Pos = globalmovepos;

                IsCollide = instance.TestCollision(transform.Pos.x, transform.Pos.y);

                // Check again if a new collision.
                if (IsCollide)
                {
                    foreach (GameObject obj in instance.GetObjects(transform.Pos.x, transform.Pos.y))
                        if (obj is ITargetable && obj.GUID != bulletdata.SenderGuid)
                            ((ITargetable)obj).OnDamage(CalculateDamage((float)instance.Rules.BulletDamage));

                    // Destroy this.
                    instance.Destroy(this);
                    break;
                }
            }
        }
        public static float CalculateDamage(float damage)
        {
            Random rand = new Random();
            return (float)Math.Round(damage + ((float)(rand.NextDouble() - 0.5 / 10) * damage), 2);
        }
    }
}
