﻿/*
 *  Player object for the connected player.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Core
{
    [Serializable]
    public class PlayerObject : GameObject, ITargetable
    {
        public override string TypeId
        {
            get { return "Player"; }
        }

        PlayerObjectData PlayerData;
        public override object Data
        {
            get { return PlayerData; }
            set { PlayerData = (PlayerObjectData)value; }
        }

        public InputData inputdata;
        public bool IsDead;
        public bool HasShot;

        public PlayerObject() { }
        public PlayerObject(Map map, Player target)
        {
            PlayerData = new PlayerObjectData { color = target.color, Name = target.Username, Health = 100.0f };
            GUID = target.GUID;
            transform = new Transform(map.DefaultSpawn, Tilt.Down);
        }

        public override void Update(GameInstance instance)
        {
            if (IsDead)
            {
                instance.Destroy(this);
                instance.Instanciante(new PlayerObject(instance.map, instance.GetPlayer(this)));
            }


            PlayerData.TookDamage = false;
            transform.tilt = inputdata.Direction;

            if (inputdata.IsMove)
            {
                bool CanMove;

                Vector2D localmovepos = new Vector2D();
                Vector2D globalmovepos;

                switch (inputdata.Direction)
                {
                    case Tilt.Up:
                        localmovepos = new Vector2D(0, -1);
                        break;
                    case Tilt.Down:
                        localmovepos = new Vector2D(0, 1);
                        break;
                    case Tilt.Left:
                        localmovepos = new Vector2D(-1, 0);
                        break;
                    case Tilt.Right:
                        localmovepos = new Vector2D(1, 0);
                        break;
                }

                globalmovepos = localmovepos + transform.Pos;

                // If that don't leave the collision map.
                if (globalmovepos.x >= 0 || globalmovepos.y >= 0 || globalmovepos.x <= instance.map.Width || globalmovepos.y <= instance.map.Height)
                    CanMove = !instance.TestCollision(globalmovepos.x, globalmovepos.y) || instance.Rules.NoClip;
                else
                    CanMove = false;

                if (CanMove)
                {
                    Log.Debug("Movement attibued for {0} at {1}", globalmovepos, GUID);
                    transform.Pos = globalmovepos;
                }
            }
            if (inputdata.IsFire && !HasShot)
            {
                Vector2D localmovepos = new Vector2D();

                switch (inputdata.Direction)
                {
                    case Tilt.Up:
                        localmovepos = new Vector2D(0, -1);
                        break;
                    case Tilt.Down:
                        localmovepos = new Vector2D(0, 1);
                        break;
                    case Tilt.Left:
                        localmovepos = new Vector2D(-1, 0);
                        break;
                    case Tilt.Right:
                        localmovepos = new Vector2D(1, 0);
                        break;
                }

                Vector2D bulletpos = transform.Pos + localmovepos;

                if (bulletpos.x >= 0 || bulletpos.y >= 0 || bulletpos.x <= instance.map.Width || bulletpos.y <= instance.map.Height)
                    instance.Instanciante(new BulletObject { transform = new Transform(bulletpos, transform.tilt), GUID = Guid.NewGuid(), bulletdata = new BulletObjectData(this) });
            }
            else
                HasShot = false;
        }
        public void OnDamage(float damage)
        {
            PlayerData.TookDamage = true;
            PlayerData.Health -= damage;
            IsDead |= PlayerData.Health <= 0;
        }
    }
}
