#!/bin/sh
# TXCobalt.Game post build script
# Add native libs and assets (Content directory).

Conf=$1
PG_Conf=$2
Outdir=../bin/TXCobalt.Game/$Conf

cp -r ../deps/bin/Pencil.Gaming/$PG_Conf/. $Outdir
cp -r ./Content ../bin/TXCobalt.Game/$Conf
