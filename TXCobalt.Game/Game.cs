﻿/*
 *  This file is a part of TXCobalt client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
using System.Threading;
using CobaltEngine;
using CobaltEngine.SimpleUI;
using Pencil.Gaming;

namespace TXCobalt.Game
{
    public class Game
    {
        public static DevConsole console;
        public static GameContent content;
        static Timer consoleUpdater;

        public void Initialize()
        {
            console = new DevConsole();

            Scene.AddScene(new MenuScene(), 0);
            Scene.LoadScene(0);
            // GameSettings.curret.viewport = new Rectangle(0, 0, DisplayManager.viewport.X * 1f, DisplayManager.viewport.Y * 1f);

            GlfwCharFun consoleFunc = (wnd, ch) =>
            {
                if (ch == '²')
                {
                    console.Active = !console.Active;
                    InputManager.SwitchChannel(console.Active ? 1 : 0);
                }
            };
            InputManager.AddFunction(consoleFunc, 1); // Console channel
            InputManager.AddFunction(consoleFunc);

            consoleUpdater = new Timer(_ => console.Update(), null, 0, 1);
        }

        public void LoadAssets()
        {
            content = content.LoadDirectory("Content"); //new GameContent("Content", true);s
        }
        public void Unload()
        {
            consoleUpdater.Dispose();
        }
            
        public void Update()
        {
            Scene.UpdateScene();
        }

        public void Draw()
        {
            Scene.DrawScene();
            console.Draw();
        }
    }
}