﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using CobaltEngine;
using Pencil.Gaming;
using Pencil.Gaming.Graphics;

namespace TXCobalt.Game
{
    /*
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new Game1())
                game.Run();
        }
    }
#endif
*/
    static class Program
    {
        [STAThread]
        static void Main()
        {
            //HACK: Fix dll error and some problems of current directory.
            Environment.CurrentDirectory = Path.GetDirectoryName(typeof(Program).Assembly.Location);

            Log.Write("TXCobalt.Game experimental version");
            GameSettings.current.SaveSettings();

            WindowManager.Init("TXCobalt.Game experimental");
            WindowManager.SetVideoMode(Glfw.GetVideoModes(Glfw.GetPrimaryMonitor()).Where(vm => vm.Width == 1280 && vm.Height == 720).ToArray()[0]);
            WindowManager.Togglefullscreen();

            GL.Viewport(0, 0, WindowManager.Width, WindowManager.Height);

            var game = new Game();
            Log.Write("Load assets...");
            game.Initialize();
            game.LoadAssets();

            Log.Write("Initialize Input Manager");
            InputManager.Init();

            double mousex, mousey;

            var updateThread = new Thread(() =>
            {
                try
                {
                    game.Update();
                }
                catch (Exception ex)
                {
                    ErrorScene.FastFail(ex);
                }
                Thread.Sleep(50);
            });
            updateThread.Start();

            // Use debugger in debug mode (with TRACE directive)
            // Out of an debug mode, give an stacktrace.
            #if !TRACE
            try
            #endif
            {
                while (!(Glfw.WindowShouldClose(WindowManager.window)))
                {
                    Glfw.PollEvents();
                    Glfw.GetCursorPos(WindowManager.window, out mousex, out mousey);


                    GL.MatrixMode(MatrixMode.Projection);
                    GL.LoadIdentity();
                    GL.Ortho(1f, 0f, 1f, 0f, 0f, 1f);

                    GL.Clear(ClearBufferMask.ColorBufferBit);
                    GL.ClearColor(Color4.Black);

                    game.Draw();

                    Glfw.SwapBuffers(WindowManager.window);
                }
                game.Unload();
            }
            #if !TRACE
            catch (Exception e)
            {
                CobaltEngine.Log.Error("The game have just crashed :( : {0}\nStacktrace :\n{1}", e.Message, e.StackTrace);

                // Reset glfw callbacks.
                Glfw.SetCharCallback(WindowManager.window, (wnd, ch) =>
                {
                });
                Glfw.SetKeyCallback(WindowManager.window, (wnd, key, scanCode, action, mods) =>
                {
                });

                ErrorScene.FastFail(e);
                while (true)
                {
                    Glfw.PollEvents();
                    GL.MatrixMode(MatrixMode.Projection);
                    GL.LoadIdentity();
                    GL.Ortho(1f, 0f, 1f, 0f, 0f, 1f);
                    GL.Clear(ClearBufferMask.ColorBufferBit);
                    GL.ClearColor(Color4.Black);
                    Scene.DrawScene();
                    Glfw.SwapBuffers(WindowManager.window);
                }
            }
            #endif
            Glfw.Terminate();
        }
    }
}
