﻿using TXCobalt.Core;

namespace TXCobalt.Game
{
    public static class TXCobaltBinder
    {
        public static void BindLog()
        {
            CobaltEngine.Log.instance.OnMessage += (message, level) => 
            {
                if(level == 0) // Debug
                    Log.Debug(message);
                else if(level==1) // Log
                    Log.Write(message);
                else if (level==2) // Warning
                    Log.Warning(message);
                else if (level==3) // Error
                    Log.Error(message);

                // Disable CobaltEngine console output.
                CobaltEngine.Log.StdoutPrinting = false;
            };
        }
    }
}

