﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
using Pencil.Gaming.MathUtils;

namespace TXCobalt.Game
{
    /// <summary>
    /// A viewpoint with support of smooth moves.
    /// </summary>
    public class Camera
    {
        public Camera()
        {
            Position = new Vector2(0);
        }

        public Camera(Vector2 position)
        {
            Position = position;
        }


        public Vector2 Position
        {
            get;
            set;
        }

        Vector2 FromIlteration;
        Vector2 ToIlteration;

        public bool IsMoveTo;
        float Amount;
        double lastTime;

        public void UpdatePosition()
        {
            
            IsMoveTo = Amount < 1f;

            if (IsMoveTo)
            {
                Position = Vector2.Lerp(FromIlteration, ToIlteration, Amount);
                Amount += (float)(Pencil.Gaming.Glfw.GetTime() - lastTime) * 10f;
                if (Amount > 1)
                    Amount = 1;
                lastTime = Pencil.Gaming.Glfw.GetTime();
            }
        }

        public void SmoothMoveTo(Vector2 To)
        {
            if (!IsMoveTo)
            {
                IsMoveTo = true;
                FromIlteration = Position;
                ToIlteration = To;
                Amount = 0;
            }
        }
    }
}
