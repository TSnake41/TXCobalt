﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System.Collections.Generic;
using System.IO;
using CobaltEngine;

namespace TXCobalt.Game
{
    /// <summary>
    /// Class with game textures.
    /// </summary>
    public struct GameContent
    {
        /// <summary>
        /// Create default GameContent.
        /// </summary>
        [System.ObsoleteAttribute("Use LoadDirectory instead.", true)]
        public GameContent(string basepath = "Content", bool FancyFonts = true)
        {
            GameObjectTextures = new Dictionary<string, Texture>
            {
                { "Player", new Texture(string.Format("{0}{1}Gameplay{1}TXCobalt_Test.png", basepath, Path.DirectorySeparatorChar)) },
                { "Bullet", new Texture(string.Format("{0}{1}Gameplay{1}BulletTexture.png", basepath, Path.DirectorySeparatorChar)) }
            };
            MapTexture = new Dictionary<int, Texture>();

            for (int i = 0; i < 127; i++)
                try
                {
                    MapTexture.Add(i, new Texture(string.Format("{0}{2}Gameplay{2}Tile{2}{1}.png", basepath, i, Path.DirectorySeparatorChar)));
                }
                catch (FileNotFoundException)
                {
                    CobaltEngine.Log.Warning("Tile {0} not found at {1}, does not use this tile.", i, string.Format("{0}{2}Gameplay{2}Tiles{2}{1}.png", basepath, i, Path.DirectorySeparatorChar));
                }

            OtherTextures = new Dictionary<string, Texture>
            {
                { "Cursor_White", new Texture(string.Format("{0}{1}UI{1}Cursor White.png", basepath, Path.DirectorySeparatorChar)) },
                { "Cursor_Black", new Texture(string.Format("{0}{1}UI{1}Cursor Black.png", basepath, Path.DirectorySeparatorChar)) },
                { "Editor_Selected", new Texture(string.Format("{0}{1}Editor{1}Selected.png", basepath, Path.DirectorySeparatorChar)) },
            };
            Fonts = new Dictionary<string, Font>
            {
                { "menu_font", new Font(string.Format(".{0}{1}UI{1}Fonts{1}Xolonium-Regular.otf", basepath, Path.DirectorySeparatorChar), FancyFonts) },
                { "console_font", new Font(string.Format(".{0}{1}UI{1}Fonts{1}Xolonium-Regular.otf", basepath, Path.DirectorySeparatorChar), FancyFonts) }
            };
        }


        public GameContent LoadDirectory(string rootpath, bool FancyFont = true)
        {
            var GameObjectsTextures = new Dictionary<string, Texture>();
            var maptextures = new Dictionary<int, Texture>();
            var otherTextures = new Dictionary<string, Texture>();
            var fonts = new Dictionary<string, Font>();

            if (!Directory.Exists(rootpath))
                throw new DirectoryNotFoundException("rootpath");

            string fulldirectorypath = Path.IsPathRooted(rootpath) ? rootpath : Path.Combine(System.Environment.CurrentDirectory, rootpath);

            string[] files =
                Directory.GetFiles(fulldirectorypath, "*", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                string filename = Path.GetFileNameWithoutExtension(file);

                char FileTypeChar = filename[0];

                // With an code using file path first caracter.
                switch (FileTypeChar)
                {
                    case 'T': // Textures
                        if(char.IsNumber(filename[2]))
                            // map texture
                            maptextures.Add(int.Parse(filename[2].ToString()), new Texture(file));
                        else if (filename[1]=='O')
                            // gameobject
                            GameObjectsTextures.Add(filename.Substring(3), new Texture(file));
                        else
                            // other texture
                            otherTextures.Add(filename.Substring(2), new Texture(file));
                        break;
                    case 'S': // Sounds
                        // TODO: Add sounds loader
                        break;
                    case 'F': // Fonts
                        fonts.Add(filename.Substring(2), new Font(file, FancyFont));
                        break;
                    default:
                        CobaltEngine.Log.Warning("Unknow file identifier : {0}", FileTypeChar);
                        break;
                }
            }

            Font.BaseFont = fonts["Base"];

            return new GameContent(GameObjectsTextures, maptextures, otherTextures, fonts);

        }

        public GameContent(Dictionary<string, Texture> gameObjectTextures, Dictionary<int, Texture> mapTexture, Dictionary<string, Texture> otherTextures, Dictionary<string, Font> fonts)
        {
            GameObjectTextures = gameObjectTextures;
            MapTexture = mapTexture;
            OtherTextures = otherTextures;
            Fonts = fonts;
        }

        internal Dictionary<string, Texture> GameObjectTextures;
        internal Dictionary<int, Texture> MapTexture;
        internal Dictionary<string, Texture> OtherTextures;
        internal Dictionary<string, Font> Fonts;
    }
}
