﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using TXCobalt.Core;
using Pencil.Gaming.MathUtils;

namespace TXCobalt.Game
{
    public class World
    {
        public World()
        {
            camera = new Camera(new Vector2(0));
            Entities = new Dictionary<Guid, Entity>();
        }

        public Dictionary<Guid, Entity> Entities { get; set; }

        public Camera camera;
        public Vector2 CameraPosition
        {
            get { return camera.Position; }
            set { camera.Position = value; }
        }

        public void TranslateCamera(Vector2 translation)
        {
            camera.SmoothMoveTo(CameraPosition + translation);
        }

        public void Update()
        {
            camera.UpdatePosition();
            foreach (Entity entity in Entities.Values)
                entity.Update();
        }

        public void Draw(GameContent gameContent)
        {
            if (Entities != null)
                foreach (Entity entity in Entities.Values)
                    entity.Draw(gameContent, this);
        }

        public void UpdateEntities(IEnumerable<GameObjectContainer> Containers)
        {
            //FIXME: Null reference exception when using Multiplayer.

            foreach (KeyValuePair<Guid, Entity> entity in Entities)
                entity.Value.Checked = false;

            foreach (GameObjectContainer container in Containers)
                if (Entities.ContainsKey(Guid.Parse(container.Guid)))
                {
                    Entity e = Entities[Guid.Parse(container.Guid)];
                    e.MoveTo(new Vector2(container.transform.Pos.x, container.transform.Pos.y));
                    e.EntityData = container;
                    e.Checked = true;
                }
                else
                {
                    if (GameSettings.EntityTypes.ContainsKey(container.TypeId))
                        Entities.Add(Guid.Parse(container.Guid),
                            (Entity)GameSettings.EntityTypes[container.TypeId].GetConstructor(new[] { typeof(GameObjectContainer) }).Invoke(new object[] { container }));
                    else
                        Entities.Add(Guid.Parse(container.Guid), new UniversalEntity(container));
                }

            List<Entity> entities = new List<Entity>(Entities.Values);

            // This part remove all non updated entities.
            foreach (Entity entity in entities.FindAll(e => !e.Checked))
                Entities.Remove(Guid.Parse(entity.EntityData.Guid));
        }
    }
}
