﻿using System;
using System.Collections.Generic;
using TXCobalt.Core;
using CobaltEngine.SimpleUI;

namespace TXCobalt.Game
{
    public class RulesEditor
    {
        public RulesEditor (Menu targetmenu)
        {
            CurrentRules = GameRules.Default;

            rulesEditor = new List<MenuItem>();

            Type ruletype = typeof(GameRules);

            var fields = ruletype.GetFields();

            foreach (var field in fields)
            {
                if (field.FieldType == typeof(string))
                    rulesEditor.Add(new MenuTextBox(field.Name, targetmenu, () => (string)field.GetValue(CurrentRules), value => field.SetValue(CurrentRules, value)));
                else if (field.FieldType == typeof(int))
                    rulesEditor.Add(new MenuNumeric(field.Name, () => (int)field.GetValue(CurrentRules), value => field.SetValue(CurrentRules, value), int.MinValue, int.MaxValue, 1, true));
                //else if (tmpfield.tmpfieldType == typeof(short))
                //    rulesEditor.Add(new MenuNumeric(tmpfield.Name, () => (int)tmpfield.GetValue(CurrentRules), value => tmpfield.SetValue(CurrentRules, value), -32768, 32767, 1, true));
                else if (field.FieldType == typeof(bool))
                    rulesEditor.Add(new MenuSwitch(field.Name, () => (bool)field.GetValue(CurrentRules), value => field.SetValue(CurrentRules, false/*(bool)value*/), true));
            }
            rulesEditor.Add(new MenuButton("Apply", () => RulesApplies(CurrentRules)));
            rulesEditor.Add(new MenuButton("Back", targetmenu.BackMenu));
        }

        public event Action<GameRules> RulesApplies = rules => {};

        public GameRules CurrentRules;
        public List<MenuItem> rulesEditor;
    }
}

