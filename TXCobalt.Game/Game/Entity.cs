﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using TXCobalt.Core;
using Pencil.Gaming.MathUtils;
using CobaltEngine;

namespace TXCobalt.Game
{
    /// <summary>
    /// Abstract class used for every entities in the client.
    /// </summary>
    public abstract class Entity
    {
        public Entity(GameObjectContainer container)
        {
            EntityData = container;
            Position = new Vector2(container.transform.Pos.x, container.transform.Pos.y);
            Checked = false;
        }

        public GameObjectContainer EntityData;
        public Vector2 Position;
        internal bool Checked;

        public abstract void Update();
        public abstract void Draw(GameContent content, World world);
        public virtual void MoveTo(Vector2 To)
        {
            Position = To;
        }
    }
    /// <summary>
    /// Basic Entity without effect with support of everything objects.
    /// </summary>
    public class UniversalEntity : Entity
    {
        public UniversalEntity(GameObjectContainer container) : base(container)
        {
            FromIlteration = ToIlteration = new Vector2(container.transform.Pos.x, container.transform.Pos.y);
            Amount = 0f;
            IsMoving = false;
            Checked = true;
        }

        protected Vector2 FromIlteration, ToIlteration;
        protected float Amount;
        protected bool IsMoving;

        public override void Update()
        {
            // Update positon.
            IsMoving = Amount <= 1f;

            if (IsMoving)
            {
                // Vector2.SmoothStep(ref FromIlteration, ref ToIlteration, Amount, out Position);
                Vector2.Lerp(ref FromIlteration, ref ToIlteration, Amount, out Position);
                // Position = (ToIlteration + FromIlteration) / 2 * Amount;
                Amount += (float)Pencil.Gaming.Glfw.GetTime() / .25f;
            }
        }
        public override void Draw(GameContent content, World world)
        {
            //EntityData = GameObjectContainer.Resolve(EntityData);

            Texture texture;
            if (!content.GameObjectTextures.TryGetValue(EntityData.TypeId, out texture))
            {
                texture = Texture.CreateEmpty(1, 1);
                CobaltEngine.Log.Error("Unknow entity texture " + EntityData.TypeId);
            }
            Rectangle drawingrect = new Rectangle(
                GameSettings.current.resX - (Position.X * GameSettings.current.TileSize + world.CameraPosition.X - GameSettings.current.resX / 2),
                Position.Y * GameSettings.current.TileSize + world.CameraPosition.Y - GameSettings.current.resY / 2,
                GameSettings.current.TileSize, GameSettings.current.TileSize
            );

            float rotation = 0;

            // Get rotation from tilt.
            switch (EntityData.transform.tilt)
            {
                case Tilt.Up:
                    rotation = MathHelper.ToRadians(180);
                    break;
                case Tilt.Down:
                    rotation = MathHelper.ToRadians(0);
                    break;
                case Tilt.Left:
                    rotation = MathHelper.ToRadians(270);
                    break;
                case Tilt.Right:
                    rotation = MathHelper.ToRadians(90);
                    break;
            }
            // Draw
            SpriteRenderer.Draw(texture, new Vector2(drawingrect.X,drawingrect.Y), new Vector2(drawingrect.X + drawingrect.Width, drawingrect.Y + drawingrect.Height), rotation);
        }
        public override void MoveTo(Vector2 To)
        {
            if (!IsMoving)
            {
                IsMoving = true;
                FromIlteration = Position;
                ToIlteration = To;
                Amount = 0;
            }

        }
    }
}
