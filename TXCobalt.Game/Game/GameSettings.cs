﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using LitJson;
using TXCobalt.Core;

namespace TXCobalt.Game
{
    /// <summary>
    /// Static class with every settings used in GameScene and MenuScene.
    /// </summary>
    public struct GameSettings
    {
        static GameSettings()
        {
            current = new GameSettings("Settings.json");
            // current.SaveSettings();
        }
        public GameSettings(string configFile)
        {
            if (File.Exists(configFile))
            {
                try
                {
                    this = JsonMapper.ToObject<GameSettings>(File.ReadAllText(configFile));
                }
                catch (Exception e)
                {
                    Log.Error("Can't import config file : {0}", e.Message);
                    Log.Write(string.Format("Stacktrace : \n{0}", e.StackTrace));
                    //HACK: Generate empty using empty file path.
                    this = new GameSettings("");
                }
            }
            else
            {
                Log.Warning("The file {0} does not exists.", configFile);
                TileSize = 32;
                MusicLevel = 1;
                MusicTheme = "Unwritten Return";
                VSync = false;
                Username = "Player";
                Host = "127.0.0.1";
                Port = 5050;
                color = Color.Gray;
                Map = "default";
                BotCount = 4;
                MaxPlayer = 16;
                Editor_mapname = "New_Map";
                Editor_MapHeight = 8;
                Editor_MapWidth = 8;
                resX = 1280; 
                resY = 1024;
            }
        }
        [JsonIgnore]
        public static GameSettings current;

        // GameScene
        public int TileSize;
        public int resX, resY;

        public int MusicLevel;
        public string MusicTheme;

        public bool VSync;

        [JsonIgnore]
        public static Dictionary<string, Type> EntityTypes = new Dictionary<string, Type>
        {
            {"Player",typeof(PlayerEntity)},
            {"Killer", typeof(UniversalEntity) }
        };

        // Menu Scene
        // Multiplayer
        public string Username, Host;
        public int Port;

        [JsonIgnore]
        public Color color;
        public int argbcolor
        { 
            get { return color.ToArgb(); }
            set { color = Color.FromArgb(value); }
        }

        // Singleplayer
        public string Map;
        public int BotCount;
        public int MaxPlayer;

        // Editor
        public string Editor_mapname;
        public int Editor_MapHeight, Editor_MapWidth;

        public void SaveSettings()
        {
            using (var file = new StreamWriter("Settings.json", false))
            {
                var writer = new JsonWriter(file);
                writer.PrettyPrint = true;
                writer.IndentValue = 4;
                JsonMapper.ToJson(this, writer);
            }
        }
    }
}
