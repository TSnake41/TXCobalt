﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Net;
using System.Net.Sockets;
using TXCobalt.Core;
using TXCobalt.Core.NMS;
using System.Threading;

namespace TXCobalt.Game
{
    public class NetworkProvider : DataProvider
    {
        public NetworkProvider(Socket s)
        {
            socket = s;
        }

        public NetworkProvider(string ip, short port)
        {
            Console.WriteLine("Initialize Networking provider...");
            socket = new Socket(IPAddress.Parse(ip).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            IP = ip;
            Port = port;
            socket.Connect(IP, Port);
            lastUpdate = new GameUpdate();

            thread = new Thread(() => 
            { 
                while (true)
                {
                    client.Send(new NetworkMessage { Flag = MessageFlag.InputData, Data = lastInput });
                    Thread.Sleep(50);
                }   
            });

        }

        public string IP;
        public short Port;
        bool Ready;
        Thread thread;

        NmsClient client;
        public Socket socket;
        GameUpdate lastUpdate;
        InputData lastInput;

        public override ServerResponse GetResponse()
        {
            Console.WriteLine("Get response from server.");

            byte[] buffer = new byte[0];
            byte[] _buffer = new byte[socket.ReceiveBufferSize];
            Console.WriteLine("Test");
            int length = socket.Receive(_buffer);
            Array.Resize(ref buffer, length);
            Array.Copy(_buffer, 0, buffer, 0, length);

            client = new NmsClient(socket);

            client.OnMessageRecieved += message =>
            {
                if (message.Flag == MessageFlag.Update)
                {
                    lastUpdate = (GameUpdate)message.Data;
                    Ready = true;
                    if(thread.ThreadState == ThreadState.Unstarted)
                        thread.Start();
                    // client.Send(new NetworkMessage() { Flag = MessageFlag.InputData, Data = lastInput });
                }
            };

            return ServerResponse.Deserialize(buffer);
        }

        public override void SendData(InputData input)
        {
            if (!Ready)
                return;

            /*
                Console.WriteLine("Client --> Server packet.");
                socket.Send(Serializer.Serialize(input));
            */
            lastInput = input;
        }

        public override void SendRequest(ConnectionRequest request)
        {
            Console.WriteLine("Send connection datas to server ...");
            socket.Send(Serializer.Serialize(request));
        }

        public override GameUpdate GetUpdate()
        {
            return !Ready ? new GameUpdate() : lastUpdate;
            
            /*
            Console.WriteLine("Server --> Client packet.");

            byte[] buffer = new byte[0];
            byte[] _buffer = new byte[socket.ReceiveBufferSize];

            int length = socket.Receive(_buffer);
            Array.Resize(ref buffer, length);
            Array.Copy(_buffer, 0, buffer, 0, length);

            return Serializer.Deserialize<GameUpdate>(buffer);
            */
        }
        public override void Dispose()
        {
            // socket.Dispose();
            //TODO: Reuse socket for better implementation.
            socket.Disconnect(false);
            socket.Dispose();
        }
    }
}
