﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using CobaltEngine;
using CobaltEngine.SimpleUI;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;
using TXCobalt.Core;

namespace TXCobalt.Game
{
    public class MenuScene : Scene
    {
        Menu menu;

        #region Menus

        public List<MenuItem> main_menu;

        public List<MenuItem> Singleplayer_menu;
        public List<MenuItem> Multiplayer_menu;
        public List<MenuItem> Options_menu;

        public List<MenuItem> Editor_menu;

        // Test menu
        public List<MenuItem> Test_menu;

        #endregion

        public int TestUpdate = 0;

        public override void Initialize()
        {
            menu = new Menu(new Rectangle(0,0, WindowManager.Width, WindowManager.Height), new Vector2(0, 600 / 4));

            RulesEditor rulesEditor = new RulesEditor(menu);

            #region Menu list
            main_menu = new List<MenuItem>
            {
                new MenuButton("Singleplayer", () => menu.LoadMenu(Singleplayer_menu)),
                new MenuButton("Multiplayer", () => menu.LoadMenu(Multiplayer_menu)),
                new MenuButton("Options", () => menu.LoadMenu(Options_menu)),
                new MenuButton("Exit", () => { Pencil.Gaming.Glfw.DestroyWindow(WindowManager.window); Environment.Exit(0); }),
                new MenuButton("Test menu", () => menu.LoadMenu(Test_menu))
            };

            Options_menu = new List<MenuItem>
            {
                new MenuSwitch("Toggle Fullscreen", () => WindowManager.Fullscreen, _ =>
                {
                }, false), // Toggle fullscreen button
                new MenuResolutionChooser("Resolution"), // Resolution slider
                new MenuSwitch("V-Sync", () => GameSettings.current.VSync, _ => GameSettings.current.VSync = _, true), // V-Sync toggle
                new MenuNumeric("TileSize", () => GameSettings.current.TileSize, _ => GameSettings.current.TileSize = _, 4, 512, 2, true), // TileSize numeric
                new MenuButton("Apply", UpdateResolution), // Apply button (save to settings file and apply rendering changes)
                new MenuButton("Back", menu.BackMenu)
            };
           
            Singleplayer_menu = new List<MenuItem>
            {
                new MenuButton("Play !", () =>
                {
                    try
                    {
                        GameInstanceManager manager = new GameInstanceManager();
                        manager.AddServer(new GameInstance(Map.ImportMap(string.Format("Maps\\{0}.map", GameSettings.current.Map)), rulesEditor.CurrentRules));
                        AddScene(new GameScene(new LocalProvider(manager), new ConnectionRequest(GameSettings.current.Username, "", "Monogame_0.2", GameSettings.current.color), true), 1);
                        // Unload(0);
                        LoadScene(1);
                    }
                    catch (Exception e)
                    {
                        ChangeScene(0);
                        CobaltEngine.Log.Error("Failled to create the server ({0})", e.Message);
                    }
                }),

                new MenuNumeric("Bot count (not working)", GameSettings.current.BotCount, 0, 64, 1, true),
                new MenuNumeric("Max players", GameSettings.current.MaxPlayer, 0, 256, 1, true),
                new MenuTextBox("Map", menu, GameSettings.current.Map),
                new MenuButton("Rules Editor", () => menu.LoadMenu(rulesEditor.rulesEditor)),
                new MenuButton("Back", menu.BackMenu)
            };
            Multiplayer_menu = new List<MenuItem>
            {
                new MenuButton("Play !", () =>
                {
                    try
                    {
                        AddScene(new GameScene(
                            new NetworkProvider(GameSettings.current.Host, (short)GameSettings.current.Port),
                            new ConnectionRequest(GameSettings.current.Username, "", "Monogame_0.2", GameSettings.current.color)), 1);

                        Unload(0);
                        LoadScene(1);
                    }
                    catch
                    {
                        CobaltEngine.Log.Error("Failled to connect to server");
                    }
                }),
                new MenuTextBox("Host", menu, () => GameSettings.current.Host, _ => GameSettings.current.Host = _),
                new MenuNumeric("Port", () => GameSettings.current.Port, _ => GameSettings.current.Port = _, 1, 65536, 10, true),
                new MenuNumeric("Red", () => GameSettings.current.color.Red, _ => GameSettings.current.color.Red = (byte)_, 0, 255, 1, true),
                new MenuNumeric("Blue", () => GameSettings.current.color.Blue, _ => GameSettings.current.color.Blue = (byte)_, 0, 255, 1, true),
                new MenuNumeric("Green", () => GameSettings.current.color.Green, _ => GameSettings.current.color.Green = (byte)_, 0, 255, 1, true),
                new MenuTextBox("Username", menu, () => GameSettings.current.Username, _ => GameSettings.current.Username = _),
                new MenuButton("Back", menu.BackMenu)
            };


            Editor_menu = new List<MenuItem>
            {
                new MenuNumeric("Map width", () => GameSettings.current.Editor_MapWidth, _ => GameSettings.current.Editor_MapWidth = _, 2, 1024, 1, true),
                new MenuNumeric("Map height", () => GameSettings.current.Editor_MapHeight, _ => GameSettings.current.Editor_MapHeight = _, 2, 1024, 1, true),
                new MenuTextBox("Map name", menu, () => GameSettings.current.Editor_mapname, _ => GameSettings.current.Editor_mapname = _),
                new MenuButton("Start editor", () =>
                {
                    try
                    {
                        AddScene(new Editor.EditorScene(new Map(new byte[GameSettings.current.Editor_MapHeight * GameSettings.current.Editor_MapWidth], 
                            GameSettings.current.Editor_MapWidth, GameSettings.current.Editor_MapHeight, new Vector2D(1, 1))), 2);
                        LoadScene(2);
                    }
                    catch
                    {
                        CobaltEngine.Log.Error("Failled to start the Editor.");
                    }
                }),
                new MenuButton("Back", menu.BackMenu)
            };

            Test_menu = new List<MenuItem>()
            {
                new MenuItem("Label"),
                new MenuItem("Update : ", _ =>
                {
                }, () =>
                {
                    TestUpdate++;
                    return "Update : " + TestUpdate;
                }),
                new MenuTextBox("Textbox", menu, "Write here !"),
                // new MenuButton("Open Console", Game.console.Open),
                new MenuButton("Goto Editor", () => menu.LoadMenu(Editor_menu)),
                new MenuButton("Back", menu.BackMenu),
                new MenuButton("Crash test", () => ErrorScene.FastFail(new Exception("TXCobalt likes done his april fools !")))
            };
            #endregion

            menu.LoadMenu(main_menu);

            BackgroundColor = new Color4(0, 0, 0, 255);
            InputManager.SwitchChannel(menu.InputChannel);
        }

        public override void Update()
        {
            menu.UpdateText();

            //FIXME: This line break menu drawing
            // menu.offset = new Vector2(0, DisplayManager.CurretVideoMode.Height);
        }

        public void UpdateResolution()
        {
            menu.viewport = new Rectangle(0, 0, WindowManager.Width, WindowManager.Height);
            // GameSettings.curret.viewport = new Rectangle(0, 0, DisplayManager.CurretVideoMode.Width, DisplayManager.CurretVideoMode.Height);
            //graphics.SynchronizeWithVerticalRetrace = GameSettings.curret.VSync;
            //graphics.ApplyChanges();
        }

        public override void Draw()
        {
            base.Draw();
            menu.Draw();
        }

        public override void Dispose()
        {
            menu.Dispose();
        }
    }
}