﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using TXCobalt.Core;
using Pencil.Gaming.MathUtils;
using Pencil.Gaming.Graphics;
using CobaltEngine;

namespace TXCobalt.Game
{
    public class PlayerEntity : Entity
    {
        public PlayerEntity(GameObjectContainer container) : base(container)
        {
            FromIlteration = ToIlteration = new Vector2(container.transform.Pos.x, container.transform.Pos.y);
            Amount = 0f;
            IsMoving = false;
            Checked = true;
        }

        public override void Draw(GameContent content, World world)
        {
            Texture texture = content.GameObjectTextures["Player"];

            Rectangle drawingrect = new Rectangle(
                GameSettings.current.resX - (Position.X * GameSettings.current.TileSize + world.CameraPosition.X - GameSettings.current.resX / 2),
                Position.Y * GameSettings.current.TileSize + world.CameraPosition.Y - GameSettings.current.resY / 2,
                GameSettings.current.TileSize, GameSettings.current.TileSize
            );

            Rectangle playernamerect = new Rectangle(
                (int)(Position.X * GameSettings.current.TileSize + world.CameraPosition.X + GameSettings.current.TileSize / 2) + GameSettings.current.TileSize,
                (int)(Position.Y * GameSettings.current.TileSize + world.CameraPosition.Y + GameSettings.current.TileSize / 2) + GameSettings.current.TileSize,
                GameSettings.current.TileSize, GameSettings.current.TileSize
            );

            float rotation = 0;

            // Get rotation from tilt.
            switch (EntityData.transform.tilt)
            {
                case Tilt.Up:
                    rotation = 180;
                    break;
                case Tilt.Down:
                    rotation = 0;
                    break;
                case Tilt.Left:
                    rotation = 270;
                    break;
                case Tilt.Right:
                    rotation = 90;
                    break;
            }
            PlayerObjectData objectdata = (PlayerObjectData)EntityData.ObjectData;

            Color4 c = new Color4(objectdata.color.Red, objectdata.color.Green, objectdata.color.Blue, objectdata.color.Alpha);

            // Draw entity
            SpriteRenderer.Draw(texture, new Vector2(drawingrect.X,drawingrect.Y), new Vector2(drawingrect.X + drawingrect.Width, drawingrect.Y + drawingrect.Height), rotation, c);
            // spriteBatch.Draw(texture, null, drawingrect, null, new Vector2(16), rotation, null, c);
        }
        protected Vector2 FromIlteration, ToIlteration;
        protected float Amount;
        protected bool IsMoving;
        double lastTime;


        public override void Update()
        {
            // Update positon.
            IsMoving = Amount < 1f;

            if (IsMoving)
            {
                //TODO: SmoothStep not available on Pencil.Gaming so update smoothing can be glitchy (slower or faster).
                // Vector2.SmoothStep(ref FromIlteration, ref ToIlteration, Amount, out Position);
                Vector2.Lerp(ref FromIlteration, ref ToIlteration, Amount, out Position);
                // Position = (ToIlteration + FromIlteration) / 2 * Math.Min(Amount,1f);
                Amount += (float)(Pencil.Gaming.Glfw.GetTime()-lastTime) * 5f;
                lastTime = Pencil.Gaming.Glfw.GetTime();
            }
        }
        public override void MoveTo(Vector2 To)
        {
            if (!IsMoving)
            {
                IsMoving = true;
                FromIlteration = Position;
                ToIlteration = To;
                Amount = 0;
            }

        }

    }
}
