﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using QuickFont;

namespace CobaltEngine
{
    public class Font
    {
        public QFont font;

        /// <summary>
        /// The default used font.
        /// </summary>
        public static Font BaseFont;

        public Font(string path, bool Fancy, float size = 12f)
        {
            bool shadow = Fancy;
            var config = new QFontBuilderConfiguration(shadow);
            config.SuperSampleLevels = Fancy ? 8 : 4;
            font = new QFont(path, size, System.Drawing.FontStyle.Regular, config);
        }
    }
}

