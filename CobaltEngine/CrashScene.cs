﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Linq;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace CobaltEngine
{
    public class ErrorScene : Scene
    {
        Exception exception;
        Font font;

        public ErrorScene(Exception e)
        {
            for (int i = 0; i < Scene.scenes.Count; i++)
                try
                {
                    Scene.Unload(Scene.scenes.Keys.ToArray()[i]);
                    i--;
                }
                catch
                {
                }
            exception = e;

            font = Font.BaseFont;
        }

        public static void FastFail(Exception e)
        {
            Scene.AddScene(new ErrorScene(e), 5);
            Scene.LoadScene(5);
        }

        public override void Draw()
        {
            FontWriter.DrawString(string.Format(":( CobaltEngine have crashed : {0}", exception.Message), font, new Vector2(10, 10), Color4.OrangeRed);
            if (exception.StackTrace != null)
            {
                FontWriter.DrawString("Stacktrace (Give this to the developers, to help correct this problem) : ", font, new Vector2(10, 50), Color4.Gold);
                FontWriter.DrawString(exception.StackTrace, font, new Vector2(10, 75), Color4.White);
            }
        }
        public override void Dispose()
        {
            
        }
    }
}

