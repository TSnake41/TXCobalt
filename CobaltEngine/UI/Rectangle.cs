﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace CobaltEngine.UI
{
    public class Rectangle : IDrawable
    {
        public Rectangle(Rectanglei rect, Color4 color)
        {
            Rect = rect;
            Color = color;
        }

        public Rectanglei Rect;
        public Color4 Color;

        public void Draw()
        {
            SpriteRenderer.DrawRectangle(new Vector2(Rect.X, Rect.Y), new Vector2(Rect.Right, Rect.Top), 0, Color);
        }
    }
}

