﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using Pencil.Gaming;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace CobaltEngine.SimpleUI
{
    /// <summary>
    /// Menu root for MenuItem list system.
    /// </summary>
    public class Menu : IDisposable
    {
        public Menu(int inputChannel = -1)
        {
            if (inputChannel == -1)
                inputChannel = InputManager.GetFreeChannel();

            menu_index = 0;
            viewport = new Rectangle(0, 0, 640, 480);
            LockIndex = false;
            Active = true;
            InputChannel = inputChannel;

            InputManager.AddFunction((wnd, key, scanCode, action, mods) =>
            {
                if (Active)
                {
                    KeyPressed(key, action, mods);
                    if (action == KeyAction.Press)
                    {
                        if (key == Key.Up && !LockIndex)
                            MenuUp();
                        else if (key == Key.Down && !LockIndex)
                            MenuDown();
                        else if (key == Key.Enter)
                            Execute(MenuKeyMode.Enter);
                        else if (key == Key.Left)
                            Execute(MenuKeyMode.Left);
                        else if (key == Key.Right)
                            Execute(MenuKeyMode.Right);
                    }
                    else if (action == KeyAction.Repeat && AllowSpam())
                    {  
                        if (key == Key.Enter)
                            Execute(MenuKeyMode.Enter);
                        else if (key == Key.Left)
                            Execute(MenuKeyMode.Left);
                        else if (key == Key.Right)
                            Execute(MenuKeyMode.Right);
                    }
                }
            }, inputChannel);
            InputManager.AddFunction((wnd, ch) =>
            {
                if(Active)
                    CharPressed(ch);
            }, inputChannel);
            Active = true;
        }

        public Menu(Rectangle? viewport = null, Vector2? offset = null, int inputChannel = -1) : this(inputChannel)
        {
            this.viewport = viewport ?? new Rectangle(0, 0, 640, 480);
            this.offset = offset ?? Vector2.Zero;
        }

        public readonly int InputChannel;

        public bool Active;
        public int menu_index;
        public List<MenuItem> menu_datas;

        public Font font
        { 
            get
            { 
                return Font.BaseFont; 
            } 
        }

        public Vector2 offset;
        public Rectangle viewport;
        public bool DoUpdate;
        public bool LockIndex;
        internal Stack<List<MenuItem>> OldMenuStack = new Stack<List<MenuItem>>();

        public event Action<char> CharPressed = ch => {};
        public event Action<Key, KeyAction, KeyModifiers> KeyPressed = (key, action, modifier) => {};

        public void Execute(MenuKeyMode mode)
        {
            menu_datas[menu_index].action(mode);
        }

        public void LoadMenu(List<MenuItem> menu)
        {
            menu_index = 0;
            OldMenuStack.Push(menu_datas);
            menu_datas = menu;
        }

        public void LoadMenu(List<MenuItem> menu, int menu_index = 0)
        {
            this.menu_index = menu_index;
        }

        public void Draw()
        {
            float selected_col = (float)Math.Max(0.1f, (Math.Abs(Math.Sin(Glfw.GetTime() * 3f))));

            for (int i = 0; i < menu_datas.Count; i++)
            {
                Color4 color = Color4.White;
                if (menu_index == i)
                    color = new Color4(selected_col, selected_col, selected_col, 1f);

                FontWriter.DrawString(menu_datas[i].Text, font, 
                    new Vector2(
                        viewport.Width / 2 - FontWriter.MesureString(menu_datas[i].Text, font).X / 2,
                        viewport.Height / 2 + i * (FontWriter.MesureString(menu_datas[i].Text, font).Y + 2)
                    ) + offset, color);
            }
        }

        public void UpdateText()
        {
            for (int i = 0; i < menu_datas.Count; i++)
            {
                menu_datas[i].Focused = i == menu_index;

                if (menu_datas[i].NeedUpdate)
                    menu_datas[i].Text = menu_datas[i].Update();
            }
        }

        public void BackMenu()
        {
            try
            {
                menu_index = 0;
                menu_datas = OldMenuStack.Pop();
            }
            catch
            {
                throw new ApplicationException("You can't back in first menu.");
            }
        }

        public void MenuUp()
        {
            if (menu_index > 0)
                menu_index--;
        }

        public void MenuDown()
        {
            if (menu_index < menu_datas.Count - 1)
                menu_index++;
        }

        public void Dispose()
        {
            foreach (var item in menu_datas)
                item.Dispose();

            InputManager.ClearCharCallback(1);
            InputManager.ClearKeyCallback(1);
        }

        bool AllowSpam()
        {
            return menu_datas[menu_index].AllowKeySpam;
        }
    }
}
