﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using Pencil.Gaming;

namespace CobaltEngine.SimpleUI
{
    /// <summary>
    /// Resolution chooser item.
    /// </summary>
    public class MenuResolutionChooser : MenuItem
    {
        public MenuResolutionChooser(string text) : base(text)
        {
            AllowKeySpam = false;
            NeedUpdate = true;

            Update = () => string.Format("{0} : {1}x{2}*{3} {4}hz", text, WindowManager.window, WindowManager.Height, 
                WindowManager.videoMode.GreenBits + WindowManager.videoMode.RedBits + WindowManager.videoMode.BlueBits, WindowManager.videoMode.RefreshRate);
            action = _ =>
            {
                GlfwVidMode curret = WindowManager.videoMode;
                var modes = Glfw.GetVideoModes(Glfw.GetPrimaryMonitor());

                int index = Array.IndexOf(modes, curret);

                switch (_)
                {
                    case MenuKeyMode.Left:
                        curret = index - 1 < 0 ? modes[modes.Length - 1] : modes[index - 1];
                        break;
                    case MenuKeyMode.Right:
                        curret = index + 1 >= modes.Length ? modes[0] : modes[index + 1];                        
                        break;
                }
                WindowManager.videoMode = curret;
            };
        }

    }
}
