﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace CobaltEngine.SimpleUI
{
    /// <summary>
    /// Numeric slider item.
    /// </summary>
    public class MenuNumeric : MenuItem
    {
        public float value, min, max, step;

        string label;
        bool drawarrows;
        
        public MenuNumeric(string text, int Value, int Min, int Max, int Step, bool DrawArrows) : base(text)
        {
            value = Value;
            min = Min; max = Max; step = Step;
            label = text;
            drawarrows = DrawArrows;
            AllowKeySpam = true;

            action = _ =>
            {
                switch (_)
                {
                    case MenuKeyMode.Left:
                        value = Math.Max(min, value - step);
                        break;
                    case MenuKeyMode.Right:
                        value = Math.Min(max, value + step);
                        break;
                }
            };
            NeedUpdate = true;

            Update = () =>
            {
                string output = "";

                if (Focused && value != min && drawarrows)
                    output += "< ";

                output += string.Format("{0} : {1}", label, value);

                if (Focused && value != max && drawarrows)
                    output += " >";

                return output;
            };
        }
        public MenuNumeric(string text, float Value, float Min, float Max, float Step, bool DrawArrows) : base(text)
        {
            value = Value;
            min = Min; max = Max; step = Step;
            label = text;
            drawarrows = DrawArrows;
            AllowKeySpam = true;

            action = _ =>
            {
                switch (_)
                {
                    case MenuKeyMode.Left:
                        value = Math.Max(min, value - step);
                        break;
                    case MenuKeyMode.Right:
                        value = Math.Min(max, value + step);
                        break;
                }
            };
            NeedUpdate = true;

            Update = () =>
            {
                string output = "";

                if (Focused && value != min)
                    output += "< ";

                output += string.Format("{0} : {1}", label, value);

                if (Focused && value != max)
                    output += " >";

                return output;
            };
        }
        public MenuNumeric(string text, Func<float> getter, Action<float> setter, float Min, float Max, float Step, bool DrawArrows) : base(text)
        {
            min = Min; max = Max; step = Step;
            label = text;
            drawarrows = DrawArrows;
            AllowKeySpam = true;

            action = _ =>
            {
                switch (_)
                {
                    case MenuKeyMode.Left:
                        setter(Math.Max(min, getter() - step));
                        break;
                    case MenuKeyMode.Right:
                        setter(Math.Min(max, getter() + step));
                        break;
                }
            };
            NeedUpdate = true;

            Update = () =>
            {
                string output = "";

                if (Focused && getter() != min)
                    output += "< ";

                output += string.Format("{0} : {1}", label, getter());

                if (Focused && getter() != max)
                    output += " >";

                return output;
            };
        }
        public MenuNumeric(string text, Func<int> getter, Action<int> setter, int Min, int Max, int Step, bool DrawArrows) : base(text)
        {
            min = Min; max = Max; step = Step;
            label = text;
            drawarrows = DrawArrows;
            AllowKeySpam = true;

            action = _ =>
            {
                switch (_)
                {
                    case MenuKeyMode.Left:
                        setter((int)Math.Max(min, getter() - step));
                        break;
                    case MenuKeyMode.Right:
                        setter((int)Math.Min(max, getter() + step));
                        break;
                }
            };
            NeedUpdate = true;

            Update = () =>
            {
                string output = "";

                if (Focused && getter() != min)
                    output += "< ";

                output += string.Format("{0} : {1}", label, getter());

                if (Focused && getter() != max)
                    output += " >";

                return output;
            };
        }
    }
}
