﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using Pencil.Gaming;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace CobaltEngine.SimpleUI
{
    public class DevConsole
    {
        public DevConsole(float YFactor = .3f, Texture Background = null)
        {
            new BasicConsole(this);

            this.YFactor = YFactor;

            if (Background == null)
            {
                var bmp = new System.Drawing.Bitmap(2, 2);
                bmp.SetPixel(0, 0, System.Drawing.Color.White);
                bmp.SetPixel(1, 0, System.Drawing.Color.White);
                bmp.SetPixel(0, 1, System.Drawing.Color.White);
                bmp.SetPixel(1, 1, System.Drawing.Color.White);

                this.Background = new Texture(bmp);
            }
            else
                this.Background = Background;

            Active = false;
            color = new Color4(0.15f, 0.15f, 0.15f, 0.75f);

            log = new List<ColoredString>();

            InputManager.AddFunction((wnd, ch) =>
            {
                CurrentInput += ch;
            }, 1);
            InputManager.AddFunction((wnd, key, scanCode, action, mods) =>
            {
                if (action == KeyAction.Press || action == KeyAction.Repeat)
                    switch (key)
                    {
                        case Key.Backspace:
                            if (CurrentInput.Length > 0)
                                CurrentInput = CurrentInput.Remove(CurrentInput.Length - 1);
                            break;
                        case Key.Enter:
                            OnPrompt(CurrentInput);
                            CurrentInput = "";
                            break;
                    }
            }, 1);

            Log.instance.OnMessage += (message, level) => 
            {
                if(level == 0) // Debug
                    log.Add(new ColoredString(message, Color4.LightGray));
                else if (level == 1) // Log
                    log.Add(new ColoredString(message, Color4.White));
                else if (level == 2) // Warning
                    log.Add(new ColoredString(message, Color4.Gold));
                else if (level == 3) // Error
                    log.Add(new ColoredString(message, Color4.Red));
            };
        }

        public List<ColoredString> log;
        public string CurrentInput = "";
        public bool Active;
        public float YFactor;
        public Color4 color;

        public Texture Background;

        public delegate void PromptCallback(string prompt);
        public event PromptCallback OnPrompt = message => {};

        int LineCount { get { return (int)(WindowManager.Height / FontWriter.MesureString("X", Font.BaseFont).X * YFactor); } }
        int RowCount { get { return (int)(WindowManager.Width / FontWriter.MesureString("X", Font.BaseFont).X); } }

        Rectangle ClosedRect { get { return new Rectangle(0, 0, WindowManager.Width, (WindowManager.Height * YFactor)); } }
        Rectangle OpenedRect { get { return new Rectangle(0, 0, WindowManager.Width, WindowManager.Height - (WindowManager.Height * YFactor)); } }

        public void Draw()
        {
            if (Active)
            {
                // Draw background
                SpriteRenderer.Draw(Background, new Vector2(0, 0), new Vector2(OpenedRect.X + OpenedRect.Width,OpenedRect.Height - (WindowManager.Height * .075f)), 0, color);

                Font font = Font.BaseFont;

                // Draw text
                List<ColoredString> lines = log.GetRange(Math.Max(0, log.Count - LineCount), Math.Min(log.Count, LineCount));
                for (int i = 0; i < lines.Count; i++)
                    FontWriter.DrawString(lines[i].Text, font, 
                        new Vector2(0, OpenedRect.Top + FontWriter.MesureString(lines[i].Text, font).Y * (i)), lines[i].Color);

                FontWriter.DrawString(string.Format("> {0}", CurrentInput), font, 
                    new Vector2(0, OpenedRect.Bottom/2 - FontWriter.MesureString("X", font).Y), Color4.White);

                var col = new Color4((float)Math.Sin(Glfw.GetTime()), (float)Math.Cos(Glfw.GetTime()), (float)Math.Sin(Glfw.GetTime() + Math.PI), 1f);

                FontWriter.DrawString(string.Format("CobaltEngine v{0}", Version.verstring), Font.BaseFont, 
                    new Vector2(OpenedRect.Right - FontWriter.MesureString("CobaltEngine v" + Version.verstring, Font.BaseFont).X, 
                        OpenedRect.Bottom/2 - FontWriter.MesureString("X", font).Y), col);
            }
        }

        public void Update()
        {
        }

        public void Open()
        {
            Active = true;
        }

        public void Close()
        {
            Active = false;
        }

        enum ConsoleState
        {
            Opened,
            Closed,
            Opening,
            Closing
        }

        static string ReadLine(Stream stream)
        {
            stream.Position = 0;
            
            var bytes = new List<byte>();
            while (true)
            {
                int b = stream.ReadByte();
                if (b == '\n')
                    break;
                if (b == -1)
                    return null;
                bytes.Add((byte)b);
            }
            stream.SetLength(0);
            stream.Flush();
            return System.Text.Encoding.UTF8.GetString(bytes.ToArray());
        }
    }
}