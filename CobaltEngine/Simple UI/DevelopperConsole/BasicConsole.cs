﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Linq;

namespace CobaltEngine.SimpleUI
{
    public class BasicConsole
    {
        public BasicConsole(DevConsole console)
        {
            this.console = console;
            console.OnPrompt += OnPrompt;
        }

        DevConsole console;

        void OnPrompt(string message)
        {
            string[] parts = message.Split(' ');

            switch (parts[0].ToLower())
            {
                case "clear":
                    console.log.Clear();
                    break;
                case "exit":
                    Environment.Exit(0);
                    break;
                case "print":
                    Log.Write(string.Join(" ", parts.Skip(1)));
                    break;
                case "help":

                    break;
                default:
                    Log.Write(string.Format("Unknow command {0}", parts[0]));
                    break;
            }
        }
    }
}

