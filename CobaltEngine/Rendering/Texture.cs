﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System.Drawing;
using Pencil.Gaming.MathUtils;
using Pencil.Gaming.Graphics;

namespace CobaltEngine
{
    public class Texture
    {
        public readonly Image texture_image;
        public Rectanglei Bounds
        {
            get
            {
                return new Rectanglei(0, 0, texture_image.Width, texture_image.Height);
            }
        }

        public Texture(string path)
        {
            texture_image = Image.FromFile(path);

        }
        public Texture(Bitmap bitmap)
        {
            texture_image = bitmap;
        }

        public static Texture CreateEmpty(int Width, int Height)
        {
            var bitmap = new Bitmap(Width, Height);
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                    bitmap.SetPixel(Width, Height, Color.Transparent);

            return new Texture(bitmap);
        }

        public int GenTexture()
        {
            return GL.Utils.LoadImage((Bitmap)texture_image);
        }
    }
}

