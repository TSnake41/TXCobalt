﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System.Drawing;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;
using QuickFont;

namespace CobaltEngine
{
    public static class FontWriter
    {
        public static void DrawString(string text, Font font, Vector2 position, Color4 color)
        {
            font.font.PushOptions(new QFontRenderOptions { Colour = color });

            QFont.Begin();
            font.font.Print(text, position);
            QFont.End();
        }
        public static Vector2 MesureString(string text, Font font)
        {
            SizeF ssize = font.font.Measure(text);
            return new Vector2(ssize.Width, ssize.Height);
        }
    }
}

