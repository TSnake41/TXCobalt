/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using Pencil.Gaming;

namespace CobaltEngine
{
    /// <summary>
    /// Manage glfw window and glfw context.
    /// </summary>
    public static class WindowManager
    {
        public static string Title;

        public static GlfwWindowPtr window;
        public static GlfwVidMode videoMode;
        public static GlfwMonitorPtr monitor;
        public static bool Fullscreen { get; private set; }

        public static int Width { get { return videoMode.Width; } }
        public static int Height { get { return videoMode.Height; } }

        /// <summary>
        /// Is the Glfw3 library initialized ?
        /// </summary>
        static bool Initialized;

        public static void Init(string title = "CobaltEngine Game Window")
        {
            if (!Initialized)
            {
                Glfw.Init();
                Initialized = true;
                videoMode = Glfw.GetVideoMode(Glfw.GetPrimaryMonitor());
            }

            Log.Write("Initialize Glfw...");
            Log.Write("Glfw {0}", Glfw.GetVersionString());

            Log.Write("Current video mode : {0}x{1}*{3} {2}hz", videoMode.Width, videoMode.Height,
                videoMode.RefreshRate, videoMode.BlueBits + videoMode.RedBits + videoMode.GreenBits);

            Title = title;

            monitor = Fullscreen ? Glfw.GetPrimaryMonitor() : GlfwMonitorPtr.Null;
            window = Glfw.CreateWindow(videoMode.Width, videoMode.Height, title, monitor, GlfwWindowPtr.Null);
            if (!Fullscreen)
                Glfw.ShowWindow(window);

            Glfw.MakeContextCurrent(window);

            Glfw.SwapInterval(1);
        }

        public static void SetVideoMode(GlfwVidMode mode)
        {
            videoMode = mode;
            Reload();
        }
        public static void Reload()
        {
            Glfw.DestroyWindow(window);
            Init(Title);
        }
        public static void Togglefullscreen()
        {
            Fullscreen = !Fullscreen;
            Reload();
        }
    }
}
