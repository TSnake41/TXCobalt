﻿/*
 *  This file is a part of Cobalt Engine.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace CobaltEngine
{
    /// <summary>
    /// Usefull class for Logging.
    /// </summary>
    public static class Log
    {
        /// <summary>
        /// Write the output to the console (stdout) ?
        /// </summary>
        public static bool StdoutPrinting = true;
        public static bool ShowDebug;

        public static LogInstance instance = new LogInstance();

        public static void Debug(string message, params object[] args)
        {
            if (ShowDebug)
            {
                message = string.Format("[DEBUG] " + message, args);
                ConsoleColor oldcolor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(message);
                Console.ForegroundColor = oldcolor;
                instance.Raise(message, 0);
            }
        }
        public static void Error(string message, params object[] args)
        {
            message = string.Format("[ERROR] " + message, args);
            ConsoleColor oldcolor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = oldcolor;
            instance.Raise(message, 3);
        }
        public static void Warning(string message, params object[] args)
        {
            message = string.Format("[WARNING] " + message, args);
            ConsoleColor oldcolor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ForegroundColor = oldcolor;
            instance.Raise(message, 2);
        }
        public static void Write(string message, params object[] args)
        {
            message = string.Format(message, args);
            Console.WriteLine(message);
            instance.Raise(message, 1);
        }
    }
    /// <summary>
    /// Instance used for Log callbacks.
    /// </summary>
    public class LogInstance
    {
        public LogInstance()
        {
            OnMessage = (m, l) => {};
        }

        /// <summary>
        /// On message callback.
        /// </summary>
        public delegate void OnMessageCallback(string message, int level);
        /// <summary>
        /// Occurs when on message was written.
        /// </summary>
        public event OnMessageCallback OnMessage;

        public void Raise(string message, int level)
        {
            OnMessage(message, level);
        }
    }
}

