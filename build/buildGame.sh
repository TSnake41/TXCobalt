#!/bin/sh
echo Build TXCobalt.Game
cd ../TXCobalt.Game
# Check if xbuild can build for the 4.5 framework, otherwise, compile with 4.0.
xbuild TXCobalt.Game.csproj /p:Configuration=Release_NET45 || xbuild TXCobalt.Game.csproj /p:Configuration=Release
