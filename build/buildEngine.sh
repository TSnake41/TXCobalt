#!/bin/sh
echo Build CobaltEngine
cd ../CobaltEngine
# Check if xbuild can build for the 4.5 framework, otherwise, compile with 4.0.
xbuild CobaltEngine.csproj /p:Configuration=Release_NET45 || xbuild CobaltEngine.csproj /p:Configuration=Release
