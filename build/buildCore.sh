#!/bin/sh
echo Build TXCobalt.Core
cd ../TXCobalt.Core
# Check if xbuild can build for the 4.5 framework, otherwise, compile with 4.0.
xbuild TXCobalt.Core.csproj /p:Configuration=Release_NET45 || xbuild TXCobalt.Core.csproj /p:Configuration=Release
