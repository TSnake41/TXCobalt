#!/bin/sh

cd ../deps/
rm -r ./bin

# Build Pencil.Gaming
echo Build Pencil.Gaming

cd Pencil.Gaming
# Remove old binaries.
rm -r ./bin
mkdir ../bin

if [ "$OSTYPE" = "^darwin" ]; then
    sh build_osx.sh
    mv ./bin/OSX/ ../bin/Pencil.Gaming
else
    sh build_linux.sh
    mv ./bin/Linux/ ../bin/Pencil.Gaming
fi

cd ..

# Build LitJson
echo Build LitJson
cd litjson
sh build.sh
mkdir ../bin/LitJSON
mv LitJSON.dll ../bin/LitJSON/LitJSON.dll
cd ..

# Build NetSerializer
echo Build NetSerializer
cd NetSerializer
sh build.sh
mkdir ../bin/NetSerializer
cp ./NetSerializer.dll ../bin/NetSerializer/.

cd ..

# Build QuickFont for TXCobalt.Game

cd ..
cd TXCobalt.Game
cd Lib/QuickFont
xbuild /p:Configuration=Release
mv bin/Release/QuickFont.dll ../.
