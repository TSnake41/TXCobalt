TXCobalt
--------

TXCobalt is an free and open source customizable 2D grid tank game.

This repository contains the game base (TXCobalt.Core), a Server (TXCobalt.Server), an game framework (CobaltEngine) and an Work-In-Progress Client (TXCobalt.Game). All of these constitute the TXCobalt Project.

Functionalities
---------------
- Core
    - Simple object-oriented entity system
    - Modifier/Rules system.
    - Modular and complete network packet management
    - Default game objects.
    - i18n support
    - And many others !
- Server
    - Settings
- Game (Work-In-Progress)
    - Settings
    - Update smoothing
    - Smart content manager.
- Game framework
    - Basic drawing.
    - Simple UI
    - (WIP) Mouse and canvas based UI
    - Scenes
    - Audio (Pencil.Gaming)
    - Font printing

----------------------------------------

Compilation
-----------

You probably need to initialize submodules

    $ git submodules init


Steps for Linux/Mac
----------
To compile, you need to have mono installed

- For Ubuntu

        $ apt-get install mono-complete

For Ubuntu, please use Xamarin depots because Ubuntu's ones are often outdated (and compilation can fail).
- For Archlinux

        $ pacman -S mono

After, you just need to launch build.sh and get the output at ./bin


Steps for Windows
-----------
The build step on Windows is more complicated than Linux and was not always up to date.
No scripts for Windows are currently available.

Steps :
 - Install Visual Studio 2015
 - Build libraries and move their outputs to : deps\bin\[dependency name] (for Pencil.Gaming, move to deps\bin\Pencil.Gaming\\{Configuration} including natives dependencies)
 - Build TXCobalt.sln and move Content directory in TXCobalt.Game to the TXCobalt.Game output.

 (Or, you can try to run build.sh with MinGW or Cygwin with mono installed)

------------
Locales
---
TXCobalt support l18n.
Just move locales directory to output and TXCobalt auto-detect the system language and locale file to use.
